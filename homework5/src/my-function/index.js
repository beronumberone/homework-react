export function closeAnimate(item) {
    document.querySelector(`.${item}`)?.animate([
        {transform: "translateY(0%)"},
        {transform: "translateY(-110%) "},
    ], {duration: 600, iterations: 1});
}


