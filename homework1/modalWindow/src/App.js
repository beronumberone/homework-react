import Wrapper from "./components/Wrapper";

const App = () => (
    <div className="App">
        <Wrapper
            className="wrapper"
            modalClass='modal'
            btnTextModalFirst='Open first modal'
            btnTextModalSecond='Open second modal'
            btnClassA="btn-a"
            btnClassB="btn-b"
            btnClassC="btn-c"
            btnClassD="btn-d"
            btnDatatypeFirst='btnFirst'
            btnDatatypeSecond='btnSecond'
        />
    </div>
);

export default App