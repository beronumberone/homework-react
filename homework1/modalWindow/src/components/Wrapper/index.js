import {Component} from 'react';
import Button from "../Button";
import Modal from "../Modal";

import "./index.scss";

function closeAnimate(item) {
    document.querySelector(`.${item}`)?.animate([
        {transform: "translateY(0%)"},
        {transform: "translateY(-110%) "},
    ], {duration: 600, iterations: 1});
}

const param = {
    modalTextBtnEn: ['Ok', 'Close'],
    modalTextBtnUk: ['Так', 'Закрити'],
    modalTitleEn: 'Do you want to delete this file?',
    modalContentEn: 'Once you delete this file, it won’t be possible to undo this action.Are you sure you want to delete it?',
    modalTitleUk: 'Ви хочете видалити цей файл?',
    modalContentUk: 'Після видалення файлу скасувати цю дію буде неможливо. Ви впевнені, що хочете видалити його?',
    modalBtnFunctionEn: [_ => console.log('Ok'), _ => console.log('Close')],
    modalBtnFunctionUk: [_ => console.log('Так'), _ => console.log('Закрити')]
}

export default class Wrapper extends Component {
    state = {
        activeModal: false,
    }

    clickOpenModalEn = _ => {
        this.setState(_ => {
            return ({
                modalTitleText: param.modalTitleEn,
                modalContentText: param.modalContentEn,
                modalBtnText: param.modalTextBtnEn,
                modalBtnFunction: param.modalBtnFunctionEn,
                modalBtnCloseActive: true,
                activeModal: true,
            })
        })
    }

    clickOpenModalUk = _ => {
        this.setState(_ => {
            return ({
                modalTitleText: param.modalTitleUk,
                modalContentText: param.modalContentUk,
                modalBtnText: param.modalTextBtnUk,
                modalBtnFunction: param.modalBtnFunctionUk,
                modalBtnCloseActive: false,
                activeModal: true
            })
        })
    }


    closeModal = () => {
        closeAnimate(this.props.modalClass)
        setTimeout(_ => {
            this.setState(_ => {
                return ({activeModal: false})
            })
        }, 500)
    }

    clickCloseModalOut = e => {
        if (e.target.classList.contains(this.props.modalClass)) {
            this.closeModal()
        }
    }

    render = _ => {
        return (
            <div className={this.props.className}>
                {
                    this.state.activeModal &&
                    <Modal
                        modalClass={this.props.modalClass}
                        clickCloseModalOut={this.clickCloseModalOut}
                        clickCloseModal={this.closeModal}
                        modalTitleText={this.state.modalTitleText}
                        btnClassC={this.props.btnClassC}
                        modalBtnCloseActive={this.state.modalBtnCloseActive}
                        modalContentText={this.state.modalContentText}
                        modalWindow="modal-window"
                        modalHeader="modal-header"
                        modalTitle="modal-title"
                        modalBody="modal-body"
                        modalContent="modal-content"
                        modalButton="modal-button"
                        modalButtonContainer={
                            <>
                                <Button
                                    btnClass={this.props.btnClassA}
                                    btnText={this.state.modalBtnText[0]}
                                    functionOnClick={() => {
                                        this.state.modalBtnFunction[0]()
                                        this.closeModal()
                                    }}
                                />
                                <Button
                                    btnClass={this.props.btnClassA}
                                    btnText={this.state.modalBtnText[1]}
                                    functionOnClick={() => {
                                        this.state.modalBtnFunction[1]()
                                        this.closeModal()
                                    }}
                                />
                            </>
                        }
                    />
                }
                <Button
                    specific={this.props.btnDatatypeFirst}
                    btnClass={this.props.btnClassB}
                    btnText={this.props.btnTextModalFirst}
                    functionOnClick={this.clickOpenModalEn}
                />
                <Button
                    specific={this.props.btnDatatypeSecond}
                    btnClass={this.props.btnClassD}
                    btnText={this.props.btnTextModalSecond}
                    functionOnClick={this.clickOpenModalUk}
                />
            </div>
        )
    }
}