import {Component} from 'react';
import Button from "../Button";
import "./index.scss";

export default class Modal extends Component {

    state = {
        modalBtnCloseActive: this.props.modalBtnCloseActive,
    }

    componentDidMount() {
        document.querySelector(`.${this.props.modalClass}`).animate([
            {transform: "translateY(-110%)"},
            {transform: "translateY(0%) "},
        ], {duration: 600, iterations: 1});
    }

    render = () => {
        return (
            <div className={this.props.modalClass} onClick={this.props.clickCloseModalOut}>
                <div className={this.props.modalWindow}>
                    <div className={this.props.modalHeader}>
                        <div className={this.props.modalTitle}>{this.props.modalTitleText}</div>
                        {
                            this.state.modalBtnCloseActive &&
                            <Button
                                btnClass={this.props.btnClassC}
                                functionOnClick={this.props.clickCloseModal}
                            />
                        }
                    </div>
                    <div className={this.props.modalBody}>
                        <div className={this.props.modalContent}>{this.props.modalContentText}</div>
                        <div className={this.props.modalButton}>
                            {this.props.modalButtonContainer}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}