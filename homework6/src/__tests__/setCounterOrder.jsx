import reducer, { setCounterOrder, defaultInitialState } from '../my-storage/mySlice'

const initialState = defaultInitialState
test('має повернути початковий стан', () => {
    expect(reducer(undefined, { type: undefined })).toEqual(initialState)
})

test("якщо прийшов масив має створити на основі його об'єктів новий", () => {
    const next = {"toggle": false, actModal: false, counterFavorite: [], counterProduct: [], openModal: {},
          order: {counterOrder: [{name: 'додані',article: "01",price: 200000,count: 1,}]},
          stand: {boolStn: true, stand: {}}, stands: {arr: [], err: "", status: ""}}

   expect(reducer(initialState, setCounterOrder([{cardTitle:'додані',article:'01',price:'200 000'}]))).toEqual(next)
})


test("якщо прийшов не масив видаляємо", () => {
    const initialState = {"toggle": false, actModal: false, counterFavorite: [], counterProduct: [], openModal: {},
        order: {
        counterOrder:
            [
                {name: 'додані',article: "01",price: 200000,count: 1},
                {name: 'додані',article: "05",price: 200000,count: 1},
                {name: 'додані',article: "07",price: 200000,count: 1},
            ]},
        stand: {boolStn: true, stand: {}}, stands: {arr: [], err: "", status: ""}}

    const next = {"toggle": false, actModal: false, counterFavorite: [], counterProduct: [], openModal: {},
        order: {
            counterOrder:
                [
                    {name: 'додані',article: "05",price: 200000,count: 1},
                    {name: 'додані',article: "07",price: 200000,count: 1},
                ]},
        stand: {boolStn: true, stand: {}}, stands: {arr: [], err: "", status: ""}}
    expect(reducer(initialState, setCounterOrder({name: 'додані',article: "01",price: 200000,count: 1}))).toEqual(next)
})