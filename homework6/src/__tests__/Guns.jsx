import React from 'react';
import {rest} from 'msw';
import {setupServer} from 'msw/node';
import {renderWithProviders} from '../my-storage/test-utils/test-utils';
import {MemoryRouter} from "react-router-dom";
import {screen, waitFor} from '@testing-library/react';
import '@testing-library/jest-dom'
import ToggleContext from "../my-context/toggle-context"

import Guns from '../components/Guns'
import {setToggle} from "../my-storage/mySlice";

const stands = [ //фіктивний масив
    {
        src: "/img/goods/Africa-Rifle-9946-Edit.jpg",
        cardTitle: "Africa Rifle 9946",
        starColor: "black",
        description: "From our client’s first measurements to the finished gun.",
        price: "150 500",
        article: "01"
    },
    {
        src: "/img/goods/Africa-Rifle-9972-Edit.jpg",
        cardTitle: "Africa Rifle 9972",
        starColor: "black",
        description: "Fixed lock double rifle, sidelock double rifle.",
        price: "200 000",
        article: "02"
    },
]

export const handlers = [
    rest.get('/source/source1.html',
        (req, res, ctx) => {
            return res(ctx.json(stands), ctx.delay(150))
        })
]

const server = setupServer(...handlers) //імітуємо запит

// Enable API mocking before tests.
beforeAll(() => server.listen())
// Reset any runtime request handlers we may add during the tests.
afterEach(() => server.resetHandlers())
// Disable API mocking after the tests are done.
afterAll(() => server.close())


test("Guns render", async () => {
    //рендерим компонент
    renderWithProviders(
        <MemoryRouter>
            <ToggleContext.Provider value={{setToggle}}>
                <Guns/>
            </ToggleContext.Provider>
        </MemoryRouter>);
    //і шукаємо що-небудь з масиву що має відображатися у списку

    expect(await screen.findByText(/Africa Rifle 9946/i)).toBeInTheDocument()
})
