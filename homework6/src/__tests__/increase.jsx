import reducer, { increase } from '../my-storage/mySlice'


const fakeObj = //передаємо об'єкт
{
    src: "/img/goods/WR-_20390-.500-Fixed-Lock-Rifle-27911-Edit-copy.jpg",
    cardTitle: "Fixed Lock Rifle 27911",
    starColor: "black",
    description: "From our classic house scroll to that seen on our prized Modèle de Grand Luxe grade.",
    price: "200000",
    article: "01"
}

const previousState ={actModal: false, counterFavorite: [], counterProduct: [], openModal: {},
    order: {
        counterOrder: [
            {name: 'додані',article: "01",price: 200000,count: 1,}//маємо стан
        ]
    },
    stand: {boolStn: true, stand: {}}, stands: {arr: [], err: "", status: ""}}

const next = {actModal: false, counterFavorite: [], counterProduct: [], openModal: {},
    order: {
        counterOrder: [
            {name: 'додані',article: "01",price: 400000,count: 2,}//повинні отримати такий стан
        ]
    },
    stand: {boolStn: true, stand: {}}, stands: {arr: [], err: "", status: ""}}

describe("increase",()=>{
    test("збільшуємо кількість цього товару", () => {
        expect(reducer(previousState, increase(fakeObj))).toEqual(next)
    })
})
