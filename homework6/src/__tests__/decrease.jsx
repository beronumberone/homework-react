import reducer, {decrease} from '../my-storage/mySlice'

const fakeObj = //передаємо об'єкт
    {
        obj: {
            src: "/img/goods/WR-_20390-.500-Fixed-Lock-Rifle-27911-Edit-copy.jpg",
            cardTitle: "Fixed Lock Rifle 27911",
            starColor: "black",
            description: "From our classic house scroll to that seen on our prized Modèle de Grand Luxe grade.",
            price: "200 000",
            article: "01"
        },
        count: {name: 'Fixed Lock Rifle 27911', article: "01", price: 1000000, count: 5,}
    }

const previousState = {
    actModal: false, counterFavorite: [], counterProduct: [], openModal: {},
    order: {
        counterOrder: [
            {name: 'Fixed Lock Rifle 27911', article: "01", price: 1000000, count: 5,}//маємо стан
        ]
    },
    stand: {boolStn: true, stand: {}}, stands: {arr: [], err: "", status: ""}
}

const next = {
    actModal: false, counterFavorite: [], counterProduct: [], openModal: {},
    order: {
        counterOrder: [
            {name: 'Fixed Lock Rifle 27911', article: "01", price: 800000, count: 4,}//повинні отримати такий стан
        ]
    },
    stand: {boolStn: true, stand: {}}, stands: {arr: [], err: "", status: ""}
}

describe("increase", () => {
    test("зменшуємо кількість цього товару", () => {
        expect(reducer(previousState, decrease(fakeObj))).toEqual(next)
    })
})