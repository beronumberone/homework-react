import Button from '../components/Button'

import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom'

describe('tests custom button',()=>{
    it('renders btnText', () => {
        render(<Button btnText={'tests custom button'}/>);
        expect(screen.getByText(/custoM buTton/i)).toBeInTheDocument()//перевіряємо присутність доданого тексту
        expect(screen.queryByText('note exist')).not.toBeInTheDocument()//якщо хочемо протестувати відсутність тексту
    });
    it('renders class', () => {
        render(<Button btnText={'tests custom button'}
                       btnClass={'container'}
        />);
        screen.debug()//виводимо в консоль компонент
    });
    it('snapshot renders', () => {
        const component = render(<Button btnText={'snapshot renders'}
                       btnClass={'container'}
        />);
        expect(component).toMatchSnapshot(); // робимо знімок
    });
})