import "./index.scss"
import Stand from "../Stand";
import {useEffect} from "react";
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/Spinner';
import {useDispatch, useSelector} from "react-redux";
import {loadStands, setCounterFavorite} from "../../my-storage/mySlice";

const Guns = ({addToCart, currencyUnit}) => {
    const dispatch = useDispatch()
    useEffect(() => {dispatch(loadStands())}, [dispatch]);
    const stands = useSelector(state => state.my.stands.arr);
    const status = useSelector(state => state.my.stands.status)
    const err = useSelector(state => state.my.stands.err);
    const setFavorite = obj => {dispatch(setCounterFavorite(obj))}
    const color = useSelector((state) => state.my.counterFavorite)
    const getColorStar = id => color.filter(_ => +_.article === +id)[0]?.starColor === "red" && "red"

    if (err) {

        return <p className='err'>{err.message}</p>

    } else if (status === 'pending') {

        return (
            <div className="my-spinner">
                <Spinner animation="grow"/>
            </div>
        )

    } else

        return (
            <>
                <main className="main">
                    <p className="main--trek">Guns & Rifle</p>
                    <div className="main--case">
                        {stands.map(stand => (
                            <Stand
                                stand={stand}
                                key={stand.article}
                                starColor={getColorStar(stand.article) || "black"}
                                currencyUnit={currencyUnit}
                                addToCart={_ => addToCart(stand)}
                                setFavorite={_=>setFavorite(stand)}
                            />
                        ))}
                    </div>
                </main>
            </>
        )
}
export default Guns

Guns.propTypes = {
    currencyUnit: PropTypes.string,
};