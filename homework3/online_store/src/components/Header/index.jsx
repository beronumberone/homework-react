import "./index.scss"
import NavBar from '../NavBar'
import Carousel from 'react-bootstrap/Carousel';
import PropTypes from "prop-types";

const Header =({clearFavorite,counterProduct,counterFavorite})=> (
        <header className="header">
                <NavBar clearFavorite={clearFavorite} counterProduct={counterProduct} counterFavorite={counterFavorite}/>
            <Carousel fade className="may-carousel">
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="/img/header/all_desktop_2.jpg"
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="/img/header/all_desktop_3.jpg"
                        alt="Second slide"
                    />
                    <Carousel.Caption>
                        <h3>Second slide label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="/img/header/all_desktop_5.jpg"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>
                            Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                        </p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </header>
    )

Header.propTypes = {
    clearFavorite: PropTypes.func,
    counterProduct: PropTypes.number,
    counterFavorite: PropTypes.number
};

export default Header
