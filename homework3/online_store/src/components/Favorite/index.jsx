import Footer from "../Footer";
import {getStands, deleteStand, lengthStorage} from "../../my-function";
import "./index.scss"
import Table from 'react-bootstrap/Table';
import Button from "../Button";
import {useState, useEffect} from "react";

const Favorite = ({   setCounterFavorite,
                      setOpenModalEn,
                      closeModal,
                      setActiveModal,
                      modalTitleText,
                      modalContentText,
                      modalBtnText,
                      btnClassA
                  }) => {
    const deleteItem = stand => {
        deleteStand(stand.article, "favorite");
        setStands(getStands("favorite"));
        setCounterFavorite(lengthStorage("favorite"))
    }

    const clickOpenModalEn = stand => {
        setOpenModalEn({
            modalTitleText: modalTitleText,
            modalContentText: modalContentText,
            modalBtnText: modalBtnText,
            modalBtnCloseActive: true,
            buttonWrp: <Button
                btnClass={btnClassA}
                btnText={modalBtnText}
                functionOnClick={_ => {
                    setTimeout(_ => {
                        deleteItem(stand)
                    }, 500)
                    closeModal()
                }}
            />
        })
        setActiveModal(true)
    }

    const [stands, setStands] = useState([])

    useEffect(_ => {
        setStands(getStands("favorite"))
    },[])

    return (
        <>
            <div className="my-table container-fluid">
                <h1>Your Favorite</h1>
                <Table striped bordered hover variant="dark">
                    <thead>
                    <tr>
                        <th>article</th>
                        <th>img</th>
                        <th>cardTitle</th>
                        <th>price</th>
                        <th>action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {stands.map(stand => (
                        <tr key={stand.article}>
                            <td className="td td-article">{stand.article}</td>
                            <td className="td td-img"><img className="td-img--pic" src={stand.src} alt=""/></td>
                            <td className="td td-title">{stand.cardTitle}</td>
                            <td className="td td-price">{stand.price}</td>
                            <td className="td td-delete">
                                <Button
                                    btnText={"delete"}
                                    btnClass="btn-d"
                                    functionOnClick={_ => clickOpenModalEn(stand)}
                                />
                                <Button
                                    btnText={"add to cart"}
                                    btnClass="btn-d"
                                />
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </div>
            <Footer/>
        </>
    )
}

Favorite.defaultProps = {
    modalTitleText: 'delete',
    modalContentText: 'Do you really want to delete?',
    modalBtnText: 'Ok',
    btnClassA: "btn-a",
};
export default Favorite