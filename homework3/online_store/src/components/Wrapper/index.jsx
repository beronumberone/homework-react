import {useState} from 'react';
import {Route, Routes} from 'react-router-dom';
import PropTypes from "prop-types";
import {closeAnimate, lengthStorage} from "../../my-function"
import "./index.scss";
import Header from "../Header";
import Home from "../Home";
import Modal from "../Modal";
import Guns from "../Guns";
import Favorite from "../Favorite";
import Cart from "../Сart";
import Button from "../Button";
import NoteFound from "../NoteFound";

const Wrapper = ({className, modalClass, modalTitleText, modalContentText, modalBtnText, btnClassC, btnClassA}) => {

    const [activeModal, setActiveModal] = useState(false);
    const [counterFavorite, setCounterFavorite] = useState(lengthStorage("favorite"));
    const [counterProduct, setCounterProduct] = useState(lengthStorage("product"));
    const [OpenModalEn, setOpenModalEn] = useState({});
    const [colorStar, setColorStar] = useState([]);

    const clickOpenModalEn = obj => {
        setOpenModalEn({
            modalTitleText: modalTitleText,
            modalContentText: modalContentText,
            modalBtnText: modalBtnText,
            modalBtnCloseActive: true,
            buttonWrp:<Button
                       btnClass={btnClassA}
                       btnText={modalBtnText}
                      functionOnClick={_ => {
                      setProduct(obj)
                    closeModal()
                }}
            />
        })
        setActiveModal(true)
    }

    const closeModal = _ => {
        closeAnimate(modalClass)
        setTimeout(_ => {
            setActiveModal(false)
        }, 500)
    }

    const clickCloseModalOut = _ => {
        if (_.target.classList.contains(modalClass)) {
            closeModal()
        }
    }

    const setFavorite = obj => {
        obj.starColor = "red"
        if (!localStorage.getItem("favorite" + obj.article)) {
            setColorStar([{
                color: 'red'
            }, ...colorStar])
            localStorage.setItem("favorite" + obj.article, JSON.stringify(obj))
            setCounterFavorite(lengthStorage("favorite"))
        }
    }

    const setProduct = obj => {
        if (!localStorage.getItem('product' + obj.article)) {
            setTimeout(_ => {
                setCounterProduct(lengthStorage("product"))
            }, 500)
            localStorage.setItem('product' + obj.article, JSON.stringify(obj))
        }
    }

    const clearFavorite = _ => {
        setColorStar([])
        setCounterFavorite(0);
        setCounterProduct(0)
        localStorage.clear()
    }

    return (
        <>
                    <div className={className}>
                        {
                            activeModal &&
                            <Modal
                                modalClass={modalClass}
                                clickCloseModalOut={clickCloseModalOut}
                                clickCloseModal={closeModal}
                                modalTitleText={OpenModalEn.modalTitleText}
                                btnClassC={btnClassC}
                                modalBtnCloseActive={OpenModalEn.modalBtnCloseActive}
                                modalContentText={OpenModalEn.modalContentText}
                                duration="600"
                                modalButtonContainer={OpenModalEn.buttonWrp}
                            />
                        }

                        <Header
                            clickOpenModalEn={clickOpenModalEn}
                            clearFavorite={clearFavorite}
                            counterProduct={counterProduct}
                            counterFavorite={counterFavorite}
                        />
                    </div>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/guns" element={
                    <Guns
                        addToCart={obj => clickOpenModalEn(obj)}
                        currencyUnit={"£"}
                        colorStar={colorStar}
                        setFavorite={obj => setFavorite(obj)}
                    />
                }/>
                <Route path="/basket" element={<Cart
                    setCounterProduct={setCounterProduct}
                    setOpenModalEn={setOpenModalEn}
                    setActiveModal={setActiveModal}
                    closeModal={closeModal}
                />}/>
                <Route path="/favorite" element={<Favorite
                    setCounterFavorite={setCounterFavorite}
                    setOpenModalEn={setOpenModalEn}
                    closeModal={closeModal}
                    setActiveModal={setActiveModal}
                />}/>
                <Route path="*" element={<NoteFound/>}/>
            </Routes>
        </>
    )
}

Wrapper.propTypes = {
    modalClass: PropTypes.string,
    modalTitleText: PropTypes.string,
    height: PropTypes.string,
    openBasket: PropTypes.func
}

Wrapper.defaultProps = {
    modalClass: "my-modal",
    className: "wrapper container-fluid",
    modalTitleText: 'Add to Shopping Cart?',
    modalContentText: 'You can add to cart or close window and add to favorites. Continue?',
    modalBtnText: 'Ok',
    btnClassA: "btn-a",
    btnClassC: "btn-c"
};

export default Wrapper