import Footer from "../Footer";
import {deleteStand, getStands, lengthStorage} from "../../my-function";
import "./index.scss"
import Table from 'react-bootstrap/Table';
import Button from "../Button";
import {useEffect, useState} from "react";

const Cart = ({
                  setCounterProduct,
                  setOpenModalEn,
                  closeModal,
                  setActiveModal,
                  modalTitleText,
                  modalContentText,
                  modalBtnText,
                  btnClassA
              }) => {
    const [stands, setStands] = useState([])
    const deleteItem = stand => {
        deleteStand(stand.article, "product");
        setStands(getStands("product"));
        setCounterProduct(lengthStorage("product"))
    }

    const clickOpenModalEn = stand => {
        setOpenModalEn({
            modalTitleText: modalTitleText,
            modalContentText: modalContentText,
            modalBtnText: modalBtnText,
            modalBtnCloseActive: true,
            buttonWrp: <Button
                btnClass={btnClassA}
                btnText={modalBtnText}
                functionOnClick={_ => {
                    setTimeout(_ => {
                        deleteItem(stand)
                    }, 500)
                    closeModal()
                }}
            />
        })
        setActiveModal(true)
    }

    useEffect(_ => {
        setStands(getStands("product"))
    }, [])

    return (
        <>
            <div className="my-table container-fluid">
                <h1>Your shopping cart</h1>
                <Table striped bordered hover variant="dark">
                    <thead>
                    <tr>
                        <th>article</th>
                        <th>img</th>
                        <th>cardTitle</th>
                        <th>price</th>
                        <th>action</th>
                    </tr>
                    </thead>
                    <tbody>
                    {stands.map(stand => (
                        <tr key={stand.article}>
                            <td className="td td-article">{stand.article}</td>
                            <td className="td td-img"><img className="td-img--pic" src={stand.src} alt=""/></td>
                            <td className="td td-title">{stand.cardTitle}</td>
                            <td className="td td-price">{stand.price}</td>
                            <td className="td td-delete">
                                <Button
                                    btnText={"delete"}
                                    btnClass="btn-d"
                                    functionOnClick={_ => clickOpenModalEn(stand)}
                                />
                                <Button
                                    btnText={"place an order"}
                                    btnClass="btn-d"
                                />
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            </div>
            <Footer/>
        </>
    )
}

Cart.defaultProps = {
    modalTitleText: 'delete',
    modalContentText: 'Do you really want to delete?',
    modalBtnText: 'Ok',
    btnClassA: "btn-a",
};
export default Cart