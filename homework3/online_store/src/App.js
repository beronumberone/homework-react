import Wrapper from "./components/Wrapper";
const App = ({className}) => (
    <div className={className}>
        <Wrapper/>
    </div>
);

export default App

App.defaultProps = {
    className: "App"
};