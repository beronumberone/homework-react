export function closeAnimate(item) {
    document.querySelector(`.${item}`)?.animate([
        {transform: "translateY(0%)"},
        {transform: "translateY(-110%) "},
    ], {duration: 600, iterations: 1});
}

export function lengthStorage(pattern) {
    let count = 0
    for (let key in localStorage) {
        if (!localStorage.hasOwnProperty(key)) continue;
        if (key.match(pattern)) count++;
    }
    return count
}

export function getColorStar(id) {
    for (let key in localStorage) {
        if (!localStorage.hasOwnProperty(key)) continue;
        if (localStorage[key].includes(`"article":"${id}"`)) {
            if (key.includes("favorite")){
                return JSON.parse(localStorage[key]).starColor;
            }
        }
    }
}

export function getStands(pattern) {
    let arr = []
    for (let key in localStorage) {
        if (!localStorage.hasOwnProperty(key)) continue;
        if (key.includes(`${pattern}`)) {
            arr.push(JSON.parse(localStorage[key]))
        }
    }
    return arr;
}

export function deleteStand(id, pattern) {
    localStorage.removeItem(pattern+id);
}
