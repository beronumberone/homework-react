import Button from "../Button";
import "./index.scss";
import {useEffect} from "react";
import PropTypes from "prop-types";

export default function Modal({
                                  modalBtnCloseActive,
                                  modalClass,
                                  duration,
                                  clickCloseModalOut,
                                  modalTitleText,
                                  btnClassC,
                                  clickCloseModal,
                                  modalContentText,
                                  modalButtonContainer
                              }) {

    useEffect(_=> {
        document.querySelector(`.${modalClass}`).animate([
            {transform: "translateY(-110%)"},
            {transform: "translateY(0%) "},
        ], {duration: +duration, iterations: 1});
    })

    return (
        <div className={modalClass} onClick={clickCloseModalOut}>
        <div className="modal-window">
            <div className="modal-header">
                <div className="modal-title">{modalTitleText}</div>
                {
                    modalBtnCloseActive &&
                    <Button
                        btnClass={btnClassC}
                        functionOnClick={clickCloseModal}
                    />
                }
            </div>
            <div className="modal-body">
                <div className="modal-content">{modalContentText}</div>
                <div className="modal-button">
                    {modalButtonContainer}
                </div>
            </div>
        </div>
        </div>
    )
}

Modal.propTypes = {
    modalBtnCloseActive: PropTypes.bool,
    modalClass: PropTypes.string,
    duration: PropTypes.string,
    clickCloseModalOut: PropTypes.func,
    modalTitleText: PropTypes.string,
    btnClassC: PropTypes.string,
    clickCloseModal: PropTypes.func,
    modalContentText: PropTypes.string,
    modalButtonContainer: PropTypes.node
};