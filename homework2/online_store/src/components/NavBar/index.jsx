import "./index.scss"
import Basket from "../Svg/Basket";
import Star from "../Svg/Star";
import PropTypes from "prop-types";

export const NavBar =({clearFavorite, counterProduct, counterFavorite, color, size})=> (
        <nav className="navbar navbar-my-bg  transparent fixed-top">
            <nav className="navbar navbar-expand-lg container-fluid">
                    <a className="navbar-brand" href="#">Guns & Rifles</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <a className="nav-link" aria-current="page" >Home</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link">Link</a>
                            </li>
                            <li className="nav-item dropdown">
                                <a className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">Dropdown</a>
                                <ul className="dropdown-menu">
                                    <li className="dropdown-li">
                                        <a onClick={clearFavorite} className="dropdown-item">Clear favorites</a>
                                    </li>
                                    <li className="dropdown-li">
                                        <a className="dropdown-item" href="#">Another action</a></li>
                                    <li>
                                    </li>
                                    <li className="dropdown-li">
                                        <a className="dropdown-item" href="#">Something else here</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div className="navbar-my-icon">
                                    <Basket
                                        color={color}
                                        width={size}
                                        height={size}
                                    />
                                    <span className="counter-icon">{counterProduct}</span>
                                    <Star
                                        color={color}
                                        width={size}
                                        height={size}
                                    />
                                    <span className="counter-icon">{counterFavorite}</span>
                                </div>
                            </li>
                        </ul>
                        <form className="d-flex search" role="search">
                            <input className="form-control me-2 input-search" type="search" placeholder="Search" aria-label="Search"/>
                            <button className="btn btn-outline-secondary btn-search" type="submit">Search</button>
                        </form>
                    </div>
            </nav>
        </nav>
    )

NavBar.propTypes = {
    clearFavorite: PropTypes.func,
    counterProduct: PropTypes.number,
    counterFavorite: PropTypes.number
}

NavBar.defaultProps = {
    color: "#fff",
    size: "1.5rem"
};
