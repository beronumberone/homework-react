import {Component} from 'react';
import "./index.scss";
import PropTypes from "prop-types";

export default class Button extends Component {
    render = () => <button
        className={this.props.btnClass}
        onClick={this.props.functionOnClick}
    >{this.props.btnText}</button>
}

Button.propTypes = {
    btnText: PropTypes.string,
    functionOnClick: PropTypes.func,
    btnClass: PropTypes.string
};