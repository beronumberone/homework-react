import {Component} from 'react';
import Button from "../Button";
import Modal from "../Modal";
import {Header} from "../Header";
import Main from "../Main";
import {closeAnimate, lengthStorage, lengthStorageProduct} from "../../my-function"
import "./index.scss";
import PropTypes from "prop-types";
import {Footer} from "../Footer";
//import { useParams } from "react-router-dom";

export default class Wrapper extends Component {

    state = {
        activeModal: false,
        colorStar: [],
        counterFavorite: lengthStorage(),
        counterProduct: lengthStorageProduct(),
    }

    clickOpenModalEn = obj => {
        this.setState(_ => {
            return ({
                modalTitleText: this.props.modalTitleText,
                modalContentText: this.props.modalContentText,
                modalBtnText: this.props.modalBtnText,
                modalBtnCloseActive: true,
                activeModal: true,
                currentObject: obj
            })
        })
    }

    closeModal = _ => {
        closeAnimate(this.props.modalClass)
        setTimeout(_ => {
            this.setState(_ => {
                return ({activeModal: false})
            })
        }, 500)
    }

    clickCloseModalOut = _ => {
        if (_.target.classList.contains(this.props.modalClass)) {
            this.closeModal()
        }
    }

    setFavorite = id => {
        this.setState(state => ({
            counterFavorite: lengthStorage(),
            colorStar: [{
                id: 'red'
            }, ...state.colorStar]
        }))
        console.log(this.state)
        localStorage.setItem(id, 'red')
    }

    setProduct = obj => {
        if (!localStorage.getItem('product' + obj.article)) {
            setTimeout(_ => {
                this.setState({
                    counterProduct: lengthStorageProduct()
                })
            }, 500)
            localStorage.setItem('product' + obj.article, JSON.stringify(obj))
        }
    }

    clearFavorite = _ => {
        this.setState(_ => ({
            colorStar: [],
            counterFavorite: 0,
            counterProduct: 0
        }))
        localStorage.clear()
    }

    render = _ => {
        return (
            <div className={this.props.className}>
                {
                    this.state.activeModal &&
                    <Modal
                        modalClass={this.props.modalClass}
                        clickCloseModalOut={this.clickCloseModalOut}
                        clickCloseModal={this.closeModal}
                        modalTitleText={this.state.modalTitleText}
                        btnClassC={this.props.btnClassC}
                        modalBtnCloseActive={this.state.modalBtnCloseActive}
                        modalContentText={this.state.modalContentText}
                        duration="600"
                        modalButtonContainer={
                            <>
                                <Button
                                    btnClass={this.props.btnClassA}
                                    btnText={this.state.modalBtnText}
                                    functionOnClick={_ => {
                                        this.setProduct(this.state.currentObject)
                                        this.closeModal()
                                    }}
                                />
                            </>
                        }
                    />
                }

                <Header
                    clickOpenModalEn={this.clickOpenModalEn}
                    clearFavorite={this.clearFavorite}
                    counterProduct={this.state.counterProduct}
                    counterFavorite={this.state.counterFavorite}
                />

                <Main
                    addToCart={obj => this.clickOpenModalEn(obj)}
                    currencyUnit={"£"}
                    colorStar={this.state.colorStar}
                    setFavorite={this.setFavorite}
                />
                <Footer/>
            </div>
        )
    }
}

Wrapper.propTypes = {
    modalClass: PropTypes.string,
    modalTitleText: PropTypes.string,
    height: PropTypes.string,
    openBasket: PropTypes.func
}

Wrapper.defaultProps = {
    modalClass: "my-modal",
    className: "wrapper container-fluid",
    modalTitleText: 'Add to Shopping Cart?',
    modalContentText: 'You can add to cart or close window and add to favorites. Continue?',
    modalBtnText: 'Ok',
    btnClassA: "btn-a",
    btnClassC: "btn-c"
};