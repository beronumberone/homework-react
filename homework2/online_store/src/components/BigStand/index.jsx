import "./index.scss"
import Star from "../Svg/Star";
import Button from "../Button";
import PropTypes from "prop-types";

const BigStand =({
                     standObj,
                     setFavorite,
                     comeBack,
                     addToCart,
                     currencyUnit,
                     starColor,
})=>{
    return(
        <div className="bigStand">
            <img src={standObj.src} className="bigStand--img-top" alt={standObj.alt}/>
            <div className="bigStand--body">
                <div className="bigStand--stance-title">
                    <h5 className="bigStand--title">
                        {standObj.cardTitle}
                    </h5>
                    <p className="bigStand--stance-elect">
                        <small>
                            favorites:
                        </small>
                        <Star
                            color={starColor}
                            width="3rem"
                            height="3rem"
                            setFavorite={setFavorite}
                        />
                    </p>
                </div>
                <p className="bigStand--stance">
                    {standObj.description}
                </p>
                <p className="bigStand--stance">
                    <small className="bigStand--text-muted">
                        Article: {standObj.article}
                    </small>
                </p>
                <div className="bigStand--button">
                            <span className="bigStand--price">
                                {currencyUnit} {standObj.price}
                            </span>
                    <div className="bigStand--button--btn"><Button functionOnClick={addToCart} btnClass="btn-a" btnText="Add to cart"/></div>
                    <div className="bigStand--button--btn"><Button functionOnClick={comeBack} btnClass="btn-a" btnText="Come back"/></div>
                </div>
            </div>
        </div>
    )
}

export default BigStand

BigStand.propTypes = {
    standObj: PropTypes.object,
    setFavorite: PropTypes.func,
    comeBack: PropTypes.func,
    addToCart: PropTypes.func,
    currencyUnit: PropTypes.string,
    starColor: PropTypes.string
};