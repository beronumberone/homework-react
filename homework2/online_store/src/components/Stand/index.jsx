import Star from "../Svg/Star";
import Button from "../Button";
import "./index.scss";
import PropTypes from "prop-types";

const Stand = ({
                   stand,
                   setFavorite,
                   addToCart,
                   watchMore,
                   currencyUnit,
                   starColor,
               }) => {

    return (
        <div className="stand">
            <img src={stand.src} className="stand--img-top" alt={stand.alt}/>
            <div className="stand--body">
                <div className="stand--stance">
                    <h5 className="stand--title">
                        {stand.cardTitle}
                    </h5>
                    <p className="stand--star">
                        <small>
                            favorites:
                        </small>
                        <Star
                            color={starColor}
                            width="1.5rem"
                            height="1.5rem"
                            setFavorite={setFavorite}
                        />
                    </p>
                </div>
                <p className="stand--stance">
                    {stand.description}
                </p>
                <p className="stand--stance">
                    <small className="stand--text-muted">
                        Article: {stand.article}
                    </small>
                </p>
                <div className="stand--button">
                            <span className="stand--price">
                                {currencyUnit} {stand.price}
                            </span>
                    <div className="stand--button-add">
                        <Button
                            functionOnClick={watchMore}
                            btnClass="btn-a"
                            btnText="Watch more"
                        />
                    </div>
                    <Button
                        functionOnClick={addToCart}
                        btnText="Add to cart"
                        btnClass="btn-d"
                    />
                </div>
            </div>
        </div>
    )
}

export default Stand

Stand.propTypes = {
    stand: PropTypes.object,
    setFavorite: PropTypes.func,
    addToCart: PropTypes.func,
    watchMore: PropTypes.func,
    currencyUnit: PropTypes.string,
    starColor: PropTypes.string
};